## Pixdel 2.0

Html5 time killer game where you need to delete pixels by lining them up by color, in horizontal, vertical or diagonal lines

## Install

- npm install
- grunt

## License

The MIT License

2015 (c) Mike Sudyn